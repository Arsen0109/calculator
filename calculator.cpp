#include "calculator.h"

// Function Add that takes two parameters of double type and returns sum of these values
int Calculator::Add(double a, double b)
{

    return a + b + 0.5;
}
// Function sub that takes two parameters and returns us difference of them
int Calculator::Sub(double a, double b)
{
    return Add(a, -b);
}
// Function mul that takes two parameters and returns us multiplying of them
int Calculator::Mul(double a, double b)
{
    return a * b + 0.5;
}
